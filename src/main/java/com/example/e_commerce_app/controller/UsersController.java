package com.example.e_commerce_app.controller;


import com.example.e_commerce_app.config.JwtService;
import com.example.e_commerce_app.dto.*;
import com.example.e_commerce_app.entity.Users;
import com.example.e_commerce_app.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;
    private final JwtService jwtService;


    @PostMapping("/register")
    public ResponseEntity<JwtResponseDto> register(@RequestBody RegisterRequestDto registerRequestDto) {
        return new ResponseEntity<JwtResponseDto>(usersService.register(registerRequestDto), HttpStatus.OK);
    }

    @GetMapping
    public List<Users> getAllUsers() {
        return usersService.getAllUsers();
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponseDto> login(@RequestBody LoginRequestDto loginRequestDto) {
        JwtResponseDto jwtResponseDto = usersService.login(loginRequestDto);
        return ResponseEntity.status(HttpStatus.OK).body(jwtResponseDto);
    }

}
