package com.example.e_commerce_app.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull(message = "name can not be null")
    String name;

    @NotNull(message = "description can not be null")
    String description;

    @NotNull(message = "image can not be null")
    String image;

    @NotNull(message = "price can not be null")
    double price;

    @ManyToOne
    @JoinColumn(name = "category_id")
    Category category;



}
