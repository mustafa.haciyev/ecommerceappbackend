package com.example.e_commerce_app.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

import static org.springframework.security.config.Customizer.withDefaults;
@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class GeneralConfig {

    private final UserDetailsService userDetailsService;
    private final JwtTokenService jwtTokenService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable);
        http.cors(AbstractHttpConfigurer::disable);
        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.authorizeHttpRequests(auth -> auth
//                .requestMatchers("/users/register").permitAll()
                .requestMatchers("users/**").permitAll()
                        .requestMatchers("/users/admin/login").permitAll()
                        .requestMatchers("profil/**").permitAll()
                        .requestMatchers("api/category/**").permitAll()
                        .requestMatchers("products/**").permitAll()

                        .anyRequest().authenticated()
        );
        http.httpBasic(withDefaults());
        http.apply(new SecurityAdapter(jwtTokenService));
        return http.build();
    }




    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userDetailsService);
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }












//    @Bean
//    public InMemoryUserDetailsManager userDetailsManager() {
//        UserDetails publicUser = User.withDefaultPasswordEncoder()
//                .username("public")
//                .password("password")
//                .authorities("PUBLIC")
//                .build();
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user")
//                .password("password")
//                .authorities("USER")
//                .build();
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("admin")
//                .password("password")
//                .authorities("ADMIN")
//                .build();
//        return new InMemoryUserDetailsManager(user,publicUser,admin);
//
//    }

}
