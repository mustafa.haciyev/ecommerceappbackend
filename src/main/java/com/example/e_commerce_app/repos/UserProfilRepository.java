package com.example.e_commerce_app.repos;

import com.example.e_commerce_app.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfilRepository extends JpaRepository<UserProfile,Long> {

}
