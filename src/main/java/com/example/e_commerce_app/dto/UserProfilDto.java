package com.example.e_commerce_app.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProfilDto {
    String firstName;
    String lastName;
    String phoneNumber;
    String address;
}
