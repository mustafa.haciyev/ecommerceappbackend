package com.example.e_commerce_app.config;



import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Component
public class JwtService {

    private Key key;

    @Value("${spring.security.key}")
    private String secret;
    private byte[] bytes;

    @PostConstruct
    public void init(){
        bytes = Decoders.BASE64.decode(secret);
        key = Keys.hmacShaKeyFor(bytes);
    }

    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token).getBody();
    }

    public Claims parseTokenLogin(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token).getBody();
    }

    public String generateToken(User user) {
        return Jwts.builder()
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofHours(5))))
                .setSubject(user.getUsername())
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean isAdmin(String token) {
        Claims claims = parseToken(token);

        if (claims.getExpiration().before(new Date())) {
            return false;
        }

        List<String> authorityList = claims.get("authority", List.class);
        return authorityList.contains("ADMIN");
    }

}
