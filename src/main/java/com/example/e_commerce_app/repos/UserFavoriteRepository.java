package com.example.e_commerce_app.repos;

import com.example.e_commerce_app.entity.UserFavorite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserFavoriteRepository extends JpaRepository<UserFavorite,Long> {
    List<UserFavorite> findByUserId(Long userId);

    void deleteByUserIdAndProductId(Long userId, Long productId);
}
