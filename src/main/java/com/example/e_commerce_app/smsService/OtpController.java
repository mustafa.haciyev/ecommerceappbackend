//package com.example.e_commerce_app.controller;
//
//import com.nexmo.client.NexmoClient;
//import com.nexmo.client.auth.TokenAuthMethod;
//import com.nexmo.client.verify.VerifyResult;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class OtpController {
//
//    private final NexmoClient nexmoClient;
//
//    @Autowired
//    public OtpController(NexmoClient nexmoClient) {
//        this.nexmoClient = nexmoClient;
//    }
//
//    @PostMapping("/send-otp")
//    public String sendOtp(@RequestBody String phoneNumber) {
//        try {
//            VerifyResult result = nexmoClient.getVerifyClient().verify(
//                    phoneNumber,
//                    "YourAppName"
//            );
//            if (result.getStatus() == VerifyResult.Status.OK) {
//                return "OTP gönderildi";
//            } else {
//                // Hata durumunda işlem
//                return "OTP gönderilemedi: " + result.getErrorText();
//            }
//        } catch (Exception e) {
//            // Hata durumunda işlem
//            return "OTP gönderilemedi: " + e.getMessage();
//        }
//    }
//}
