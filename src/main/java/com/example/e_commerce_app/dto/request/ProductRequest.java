package com.example.e_commerce_app.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @NotNull(message = "name can not be null")
    String name;

    @NotNull(message = "description can not be null")
    String description;

    @NotNull(message = "image can not be null")
    String image;

    @NotNull(message = "price can not be null")
    double price;

    @NotNull(message = "categoryName can not be null")
    String categoryName;


}
