package com.example.e_commerce_app.controller;


import com.example.e_commerce_app.entity.Category;
import com.example.e_commerce_app.handles.ResponseHandler;
import com.example.e_commerce_app.service.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<List<Category>> getAllCategories() {
        List<Category> categories = categoryService.getAllCategories();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addCategory(@RequestBody @Valid Category category){
        try {
            Category addedProduct = categoryService.addCategory(category);
            return ResponseHandler.handleResponse("Successfully add product", HttpStatus.OK,addedProduct);
        }catch (Exception e){
            return ResponseHandler.handleResponse("ERROR", HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }


    @PutMapping("/edit/{id}") // Path parametre olarak id alınmalı
    public ResponseEntity<Object> editCategory(@PathVariable Long id, @RequestBody @Valid Category category){
        try {
            // Kategori id'sini belirle
            category.setId(id);
            Category editedCategory = categoryService.editCategory(category); // Kategoriyi güncelle
            if(editedCategory != null){
                return ResponseHandler.handleResponse("Successfully edit category", HttpStatus.OK, editedCategory);
            } else {
                return ResponseHandler.handleResponse("Category not found", HttpStatus.NOT_FOUND, null); // Kategori bulunamadı durumu
            }
        } catch (Exception e){
            return ResponseHandler.handleResponse("Error editing category", HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteCategory(@PathVariable Long id){
        try {
            categoryService.deleteCategory(id);
            return ResponseHandler.handleResponse("Successfully delete product", HttpStatus.OK,null);
        }catch (Exception e){
            return ResponseHandler.handleResponse("ERROR", HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }



}
