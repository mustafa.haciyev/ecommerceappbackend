package com.example.e_commerce_app.service;

import com.example.e_commerce_app.dto.request.ProductRequest;
import com.example.e_commerce_app.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProductService {



    Product editProduct(Product product);

    void deleteProduct(Long id);

    Page<Product> getRequestFilters(int page, int limit, String productName, Sort.Direction sortType);



    Product addProduct(ProductRequest productRequest);

    List<Product> getAll();
}
