package com.example.e_commerce_app.service.impl;

import com.example.e_commerce_app.entity.Users;
import com.example.e_commerce_app.repos.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        log.info("Username is : {}", email);

        Users users = userRepo.findByEmail(email).
                orElseThrow(() -> new RuntimeException("Email not found"));
        return User.builder()
                .username(users.getEmail())
                .password(users.getPassword())
                .build();

    }

}
