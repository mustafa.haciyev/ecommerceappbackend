package com.example.e_commerce_app.service.impl;


import com.example.e_commerce_app.config.JwtService;
import com.example.e_commerce_app.dto.JwtResponseDto;
import com.example.e_commerce_app.dto.LoginRequestDto;
import com.example.e_commerce_app.dto.RegisterRequestDto;
import com.example.e_commerce_app.entity.Users;
import com.example.e_commerce_app.repos.UserRepo;
import com.example.e_commerce_app.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UserRepo userRepo;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtService jwtService;
//    private final AuthorityRepo authorityRepo;

    private final UserDetailsService userDetailsService;

    @Override
    public JwtResponseDto register(RegisterRequestDto registerRequestDto) {

        userRepo.findByEmail(registerRequestDto.getEmail())
                .ifPresent(users -> {
                    throw new RuntimeException("User not found");
                });

        Users user = Users.builder()
                .password(passwordEncoder.encode(registerRequestDto.getPassword()))
                .email(registerRequestDto.getEmail())
                .build();
        Users saveUser = userRepo.save(user);


        UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
                .username(saveUser.getEmail())
                .password(saveUser.getPassword())
                .build();

        String token = jwtService.generateToken((org.springframework.security.core.userdetails.User) userDetails);
        return JwtResponseDto.builder()
                .jwt(token)
                .build();
    }

    @Override
    public List<Users> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public JwtResponseDto login(LoginRequestDto loginRequestDto) {
        Users user = userRepo.findByEmail(loginRequestDto.getEmail())
                .orElseThrow(() -> new RuntimeException("User not found"));

        if (passwordEncoder.matches(loginRequestDto.getPassword(), user.getPassword())) {
            UserDetails userDetails = new User(user.getEmail(), user.getPassword(), new ArrayList<>());

            String token = jwtService.generateToken((User) userDetails);
            return JwtResponseDto.builder()
                    .jwt(token)
                    .build();
        } else {
            throw new RuntimeException("Invalid credentials");
        }
    }


}




