package com.example.e_commerce_app.service;


import com.example.e_commerce_app.dto.JwtResponseDto;
import com.example.e_commerce_app.dto.LoginRequestDto;
import com.example.e_commerce_app.dto.RegisterRequestDto;
import com.example.e_commerce_app.entity.Users;

import java.util.List;

public interface UsersService {

    JwtResponseDto register(RegisterRequestDto registerRequestDto);


    JwtResponseDto login(LoginRequestDto loginRequestDto);

    List<Users> getAllUsers();
}
