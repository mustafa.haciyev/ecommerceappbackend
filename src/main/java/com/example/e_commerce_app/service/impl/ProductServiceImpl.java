package com.example.e_commerce_app.service.impl;

import com.example.e_commerce_app.dto.request.ProductRequest;
import com.example.e_commerce_app.entity.Category;
import com.example.e_commerce_app.entity.Product;
import com.example.e_commerce_app.repos.CategoryRepository;
import com.example.e_commerce_app.repos.ProductRepository;
import com.example.e_commerce_app.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;


    @Override
    public Product addProduct(ProductRequest productRequest) {
        String categoryName = productRequest.getCategoryName();
        if (categoryName == null || categoryName.isEmpty()) {
            throw new IllegalArgumentException("Kategori adı boş olamaz");
        }

        Category category = categoryRepository.findByCategoryName(categoryName);
        if (category == null) {
            category = categoryRepository.save(new Category(categoryName));
        }

        Product product = new Product();
        product.setName(productRequest.getName());
        product.setDescription(productRequest.getDescription());
        product.setImage(productRequest.getImage());
        product.setPrice(productRequest.getPrice());
        product.setCategory(category);

        return productRepository.save(product);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

//    @Override
//    public Product addProduct(Product product) {
//        Category category = product.getCategory();
//        if (category == null) {
//            throw new IllegalArgumentException("Ürün bilgisinde kategori bulunmuyor");
//        }
//
//        if (category.getId() != null) {
//            Category existingCategory = categoryRepository.findById(category.getId())
//                    .orElseThrow(() -> new IllegalArgumentException("Kategori bulunamadı: " + category.getId()));
//            product.setCategory(existingCategory);
//        } else if (category.getCategoryName() != null) {
//            Category existingCategory = categoryRepository.findByCategoryName(category.getCategoryName());
//            if (existingCategory == null) {
//                existingCategory = categoryRepository.save(category);
//            }
//            product.setCategory(existingCategory);
//        } else {
//            throw new IllegalArgumentException("Kategori bilgisi eksik");
//        }
//
//        if (product.getPrice() < 0) {
//            throw new IllegalArgumentException("Ürün fiyatı negatif olamaz");
//        }
//
//        return productRepository.save(product);
//    }

// Daha fazla düzenleme yapılabilir, ancak bu örnek size bir başlangıç sağlar.

    @Override
    public Product editProduct(Product product) {
        boolean exist = productRepository.existsById(product.getId());
        if(exist){
            return productRepository.save(product);
        }
        return null;
    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> getRequestFilters(int page,int limit,String productName, Sort.Direction sortType) {
        Page<Product> productPage = null;
        if(productName==null && sortType==null){
            productPage = getProductsList(page,limit);
        }
        if(productName!=null && sortType==null ){
            productPage = findProductsByName(page,limit,productName);
        }
        if(productName==null && sortType != null ){
            productPage = getProductsOrderByPrice(page,limit,sortType);
        }
        if(productName!=null && sortType!=null){
            productPage = findProductsByNameAndOrderByPrice(page,limit,productName,sortType);
        }
        return  productPage;
    }

    private Page<Product> getProductsList(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        return productRepository.findAll(pageable);
    }

    private Page<Product> findProductsByName(int page,int limit,String productName) {
        Pageable pageable = PageRequest.of(page, limit);
        return productRepository.findByNameContainingIgnoreCase(productName, pageable);
    }


    private Page<Product> getProductsOrderByPrice(int page, int limit,Sort.Direction sortType) {
        Sort sort = Sort.by(sortType, "price");
        Pageable pageable = PageRequest.of(page, limit,sort);
        return productRepository.findAll(pageable);
    }

    private Page<Product> findProductsByNameAndOrderByPrice(int page, int limit,
                                                            String productName,
                                                            Sort.Direction sortType) {
        Sort sort = Sort.by(sortType, "price");
        Pageable pageable = PageRequest.of(page, limit,sort);
        return productRepository.findByNameContainingIgnoreCase(productName,pageable);
    }
}
