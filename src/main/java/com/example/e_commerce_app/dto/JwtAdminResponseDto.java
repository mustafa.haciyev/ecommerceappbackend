package com.example.e_commerce_app.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JwtAdminResponseDto {
    private String token;

    public JwtAdminResponseDto(String token) {
    }

    public void JwtResponseDto() {
    }

    public void JwtResponseDto(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
