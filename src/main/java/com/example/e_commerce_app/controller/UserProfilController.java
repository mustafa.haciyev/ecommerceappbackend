package com.example.e_commerce_app.controller;

import com.example.e_commerce_app.dto.UserProfilDto;
import com.example.e_commerce_app.service.UserProfilService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profil")
@RequiredArgsConstructor
public class UserProfilController {
    private final UserProfilService userProfilService;

    @PostMapping("/add")
    public ResponseEntity<String> addProfil(@RequestBody UserProfilDto userProfilDto) {
        userProfilService.save(userProfilDto);
        return ResponseEntity.ok("Add success");
    }
}
