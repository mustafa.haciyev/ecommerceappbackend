package com.example.e_commerce_app.controller;

import com.example.e_commerce_app.dto.UserFavoriteDto;
import com.example.e_commerce_app.dto.request.AddFavoriteRequest;
import com.example.e_commerce_app.dto.request.RemoveFavoriteRequest;
import com.example.e_commerce_app.entity.Product;
import com.example.e_commerce_app.entity.UserFavorite;
import com.example.e_commerce_app.entity.Users;
import com.example.e_commerce_app.service.UserFavoriteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/favorite")
@RequiredArgsConstructor
public class UserFavoriteController {

    private final UserFavoriteService userFavoriteService;

    @PostMapping("/add")
    public ResponseEntity<?> addFavorite(@RequestBody AddFavoriteRequest request) {
        userFavoriteService.addFavorite(request.getUserId(), request.getProductId());
        return ResponseEntity.ok("Favorite added successfully.");
    }

    @DeleteMapping("/remove")
    public ResponseEntity<?> removeFavorite(@RequestBody RemoveFavoriteRequest request) {
        userFavoriteService.removeFavorite(request.getUserId(), request.getProductId());
        return ResponseEntity.ok("Favorite removed successfully.");
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<Product>> getFavoritesByUser(@PathVariable Long userId) {
        List<Product> favorites = userFavoriteService.getFavoritesByUser(userId);
        return ResponseEntity.ok(favorites);
    }
}
