package com.example.e_commerce_app.service.impl;

import com.example.e_commerce_app.dto.UserProfilDto;
import com.example.e_commerce_app.entity.UserProfile;
import com.example.e_commerce_app.repos.UserProfilRepository;
import com.example.e_commerce_app.service.UserProfilService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserProfilServiceImpl implements UserProfilService {
    private final UserProfilRepository userProfilRepository;


    @Override
    public void save(UserProfilDto userProfilDto) {
        UserProfile userProfile = UserProfile.builder()
                .firstName(userProfilDto.getFirstName())
                .lastName(userProfilDto.getLastName())
                .phoneNumber(userProfilDto.getPhoneNumber())
                .address(userProfilDto.getAddress())
                .build();
        userProfilRepository.save(userProfile);
    }
}
