//package com.example.e_commerce_app.smsService;
//
//import com.vonage.client.sms.SmsSubmissionResponse;
//import com.vonage.client.sms.messages.TextMessage;
//import org.springframework.stereotype.Service;
//
//@Service
//public class SmsService {
//
//    private final String API_KEY = "YOUR_NEXMO_API_KEY";
//    private final String API_SECRET = "YOUR_NEXMO_API_SECRET";
//
//    public void sendOtp(String phoneNumber, String otp) {
//        NexmoClient client = NexmoClient.builder()
//                .apiKey(API_KEY)
//                .apiSecret(API_SECRET)
//                .build();
//
//        TextMessage message = new TextMessage("YourAppName", phoneNumber, "Your OTP is: " + otp);
//
//        try {
//            SmsSubmissionResponse response = client.getSmsClient().submitMessage(message);
//            System.out.println("SMS sent: " + response);
//        } catch (Exception e) {
//            System.err.println("Error while sending SMS: " + e.getMessage());
//        }
//    }
//}
