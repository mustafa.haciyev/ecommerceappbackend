package com.example.e_commerce_app.repos;


import com.example.e_commerce_app.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<Users,Long> {

    Optional<Users> findByEmail(String email);
    List<Users> findAll();

}
