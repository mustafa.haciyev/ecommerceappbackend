package com.example.e_commerce_app.service;

import com.example.e_commerce_app.dto.UserFavoriteDto;
import com.example.e_commerce_app.entity.Product;

import java.util.List;

public interface UserFavoriteService {


    void addFavorite(Long userId, Long productId);

    void removeFavorite(Long userId, Long productId);

    List<Product> getFavoritesByUser(Long userId);
}
