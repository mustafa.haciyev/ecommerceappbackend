package com.example.e_commerce_app.service.impl;

import com.example.e_commerce_app.dto.UserFavoriteDto;
import com.example.e_commerce_app.entity.Product;
import com.example.e_commerce_app.entity.UserFavorite;
import com.example.e_commerce_app.entity.Users;
import com.example.e_commerce_app.mapstruct.UserFavoriteMapper;
import com.example.e_commerce_app.repos.UserFavoriteRepository;
import com.example.e_commerce_app.service.UserFavoriteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserFavoriteServiceImpl implements UserFavoriteService {
    private final UserFavoriteRepository userFavoriteRepository;
    private final UserFavoriteMapper userFavoriteMapper;


    @Override
    public void addFavorite(Long userId, Long productId) {
        UserFavoriteDto userFavorite = new UserFavoriteDto();
        userFavorite.setUserId(userId);
        userFavorite.setProductId(productId);
        userFavoriteRepository.save(userFavoriteMapper.mapRequestToUserFavorite(userFavorite));
    }

    @Override
    public void removeFavorite(Long userId, Long productId) {
        userFavoriteRepository.deleteByUserIdAndProductId(userId, productId);

    }

    @Override
    public List<Product> getFavoritesByUser(Long userId) {
        List<UserFavorite> userFavorites = userFavoriteRepository.findByUserId(userId);
        List<Product> favorites = userFavorites.stream().map(UserFavorite::getProduct).collect(Collectors.toList());
        return favorites;    }
}
