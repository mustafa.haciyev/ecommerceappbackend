package com.example.e_commerce_app.service;

import com.example.e_commerce_app.dto.UserProfilDto;

public interface UserProfilService {
    void save(UserProfilDto userProfilDto);
}
