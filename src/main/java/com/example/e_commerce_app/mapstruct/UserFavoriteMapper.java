package com.example.e_commerce_app.mapstruct;

import com.example.e_commerce_app.dto.UserFavoriteDto;
import com.example.e_commerce_app.entity.UserFavorite;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface UserFavoriteMapper {
    //    @Mapping(target = "users", ignore = true) // Bu kısmı ekle
//    Order mapRequestToOrder(OrderRequestDto orderRequestDto);

    UserFavorite mapRequestToUserFavorite(UserFavoriteDto userFavoriteDto);

//    OrderResponseDto mapEntitytoResponse(Order order);

//    OrderItem mapRequestToOrderItem(OrderItemRequestDto orderItemRequestDto);

}
